#  install packages
```
#  create .venv
    : recommended - require pipenv and python 3.6+ bit.ly/nnpipenv
    pipenv --python=3 install
    
    : the old way
    sudo python3 -m pip install virtualenv
        virtualenv -p python3 .venv/
            ./venv3/bin/pip install -r requirements.txt
```


#  alembic tutorial 
ref. http://alembic.zzzcomputing.com/en/latest/tutorial.html

##  init
```
pipenv run alembic init db/alembic
#       .    . folder name to contain migration scripts
```
should see 
  Creating directory /your/repo/db/alembic                ...  done
  Creating directory /your/repo/db/alembic/versions       ...  done
  Generating         /your/repo/db/alembic/env.py         ...  done
  Generating         /your/repo/db/alembic/README         ...  done
  Generating         /your/repo/db/alembic/script.py.mako ...  done
  Generating         /your/repo/alembic.ini               ...  done
  Please edit `configuration/connection/logging` settings in /your/repo/`alembic.ini` before proceeding.


##  alembic.ini syntax  
- file_template = %%(rev)s_%%(slug)s
> revision id       %%(rev)s 
revision message  %%(slug)s
timestamp field   %%(year)d, %%(month).2d, %%(day).2d, %%(hour).2d, %%(minute).2d, %%(second).2d

- script_location = alembic
> the folder location of the Alembic environment

- timezone = SGT
- sqlalchemy.url = driver://user:pass@localhost/dbname

## create new migration
steps to run
```bash
# step 1 - create new revision/migration file
alembic revision -m 'create account table'

: step 2 - edit the upgrade() and downgrade()

# step 3 - run it ; you may want to set the :head to run from
alembic upgrade head
```

what-happened in detail
> Alembic first checked if table *alembic_version* exists, and if not, created it
Search the current version in this table, if any, 
and then calculates *revision-path* from *head* version/revision to the revision requested  
For each revision in the path, *upgrade()* method is called

Q:what is current version/revision, how to know it, what indicate its value?
A0: command to print current revision `alembic current`
A1: the single-cell table :alembic_version that stores this value

## create 2nd migration
```bash
alembic revision -m 'Add a column'
: # edit the upgrade() and downgrade()
alembic upgrade head 
```
  
    
# misc note

## all alembic op commands
ref. http://alembic.zzzcomputing.com/en/latest/ops.html# ops

## working with revision
```bash
alembic upgrade   ae1
alembic upgrade   +2
alembic downgrade -1
alembic upgrade   ae1+2

# rewind from the beginning and come back to current
alembic downgrade base # back to the beginning
alembic upgrade   head # up to the latest

alembic revision -m 'migration name'  # create new revision

alembic current # view current revision

alembic history            # view history
alembic history --verbose  # view history
alembic history -r-3:      # view last 3 revision to current; more range syntax ref. http://alembic.zzzcomputing.com/en/latest/tutorial.html# viewing-history-ranges
```

## running plain/raw sql in migration up-down
ref. https://www.johbo.com/2016/data-migrations-with-alembic-plain-sql.html
sample code
```
def upgrade():
sql_up = '''
    YOUR SELECT QUERY
'''
c=op.get_bind(); c.execute(sql_up)
```
comment
> Quite often a good old plain SQL statement seems to be good enough to get the job done
> Drawback: 
Limited to support multiple database systems due to different sql dialects are used --> cannot write a single plain sql for that. 
It is then better to use a higher abstraction level

## get base/beginning revision
```
alembic_base_revision=`pipenv run  alembic history | grep base | cut -d'>' -f3 | cut -d' ' -f2`
    pipenv run  alembic stamp $alembic_base_revision
```


## list current alembic-heads 
```
alembic branches --verbose
alembic heads
```

## multiple head conflict
Multiple head conflict 
aka git branch merging vs alembic revision merging

When we merge git branches, we often get into situation where 
`alembic upgrade head` results as **FAILED: Multiple head revisions** error
> $ alembic upgrade head
  FAILED: Multiple head revisions are present for given argument 'head'; 
  please specify a specific target revision, '<branchname>@head' to narrow to a specific head, or 'heads' for all heads

in the error, we can see 
- option 01 to specify <branchname>@head
- option 02 to specify heads to run all head ie. `alembic upgrade heads` (note plural heads) [ref.](http://alembic.zzzcomputing.com/en/latest/branches.html#referring-to-all-heads-at-once)
  going for this option will create `complex revision tree` with lots of heads 
  --> we don't want that ie. we want to keep the **revision tree simple with one head only** 

alembic also support 
- option 03 merge heads [ref.](http://alembic.zzzcomputing.com/en/latest/branches.html#merging-branches)
  i.e. `alembic merge -m "merging revision note here" rev01 rev02` where rev01, rev02 are the two heads' revisions
  to list current heads use `alembic heads` --> get hash value for rev01, rev02 --> call `alembic merge` to merge them
  after merged, we can call `alembic upgrade head` normally
  
**conclusion**
when merging code, please proceed `alembic merge` if multiple-heads error occurs

### sample scenario 
> Sometimes, multiple engineers may create `alembic` revisions at the same time
  This creates `alembic` branching, just like in `git`
  How to resolve this problem?

The scenario
```bash
$ alembic history
bfef27249701 -> 606d0340ff68, Message 1
bfef27249701 -> 2c2b188d2dcc, Message 2
1a7b4e139431 -> bfef27249701 (branchpoint), Message 3
```
You can see that *line1* and *line2* both have the same parent revision `bfef27249701`
We can *merge* them with this command `alembic merge heads`
And alembic will create this
```bash
$ alembashic history
606d0340ff68, 2c2b188d2dcc -> ec30fec4269d (head) (mergepoint), empty message
bfef27249701 -> 606d0340ff68, Add bank info to user table
bfef27249701 -> 2c2b188d2dcc, sponsor name standardized
1a7b4e139431 -> bfef27249701 (branchpoint), Add email column to bills table```
```

There will be `merge` revision like a git `merge commit`
then you can continue to run `alembic upgrade head` on your servers in order to get *both* revisions applied to your Postgres


## create new migration file in sub-folder YYmm as db/alembic/versions/YYmm
- normally new revision file, ie created by `alembic revision -m 'some message' `, 
  will be created in the same folder with previous revision
  
- when we want to create that file in a folder eg `db/alembic/versions/YYmm`, we can proceed
  * update alembic.ini - **add YYmm** to the end of `version_locations` 
    eg 
    ```
    version_locations=versions %(rh)s/1803 %(rh)s/1804 %(rh)s/YYmm
    #                 1       2           3           4
    ``` 
  * after that, do adding new revision as normally but this time with param `--version-path`
    ie
    ```
    alembic revision -m 'some message' --version-path 'choose a folder listed in :version_locations'
    #       .        .                 !               
    ``` 
  * in other words
    00 tell alembic where to seek for revision file by listing folder paths in `version_locations`
    01 choose where to save new revision by param `--version-path`  
    ```
    alembic revision -m 'message' --version-path /a/folder/listed/in/:version_locations
    eg
    alembic revision -m 'message' --version-path alembic/versions
    alembic revision -m 'message' --version-path alembic/versions/2018
    ```

sample config in alembic.ini
```
; define variable :rh stands for revision_home
rh = %(here)s/%(script_location)s/versions
#    !        !                   .
#             script folder aka foldername when calling `alembic init foldername`
#    alembic.ini home

; define version_locations using vaiable %(rh)s above ie. grouping revision files to folder (YYYY) or (YYYYmm) by listing it below
version_locations = %(rh)s     %(rh)s/2019 %(rh)s/2020 %(rh)s/202011
#                   above :rh         YYYY        YYYY        YYYYmm
```
